﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BabySitterCalculator;

namespace BabySitterCalculator.Tests
{
    [TestClass]
    public class CLIHelperTests
    {
        SitterCalculator testCalc = new SitterCalculator();
      
        [TestMethod]
        public void VerifyUserTimeInputIsValidUsingTheSitterCalculatorMethods()
        {
            CLIHelper testHelper = new CLIHelper(testCalc);
            Assert.AreEqual(true, testHelper.VerifyUserInputTimeIsValid("5:00 PM"));
        }
        [TestMethod]
        public void CalculateMoniesEarnedForUserInputsFromCLI()
        {
            CLIHelper testHelper = new CLIHelper(testCalc);
            Assert.AreEqual(84, testHelper.CalculateMoniesEarnedFromCLIInputs("5:00 PM","8:00 PM","1:00 AM"));
        }

        [TestMethod]
        public void VerifyUserTimeInputsAreCorrectFormat()
        {
            CLIHelper testHelper = new CLIHelper(testCalc);
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("5"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("503"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("5:03"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("11:03"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("503 PM"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("5:03 P"));
            Assert.AreEqual(false, testHelper.VerifyTimeFormat("5:03PM"));
        }
    }
}
