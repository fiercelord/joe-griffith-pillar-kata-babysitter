﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BabySitterCalculator;

namespace BabySitterCalculator.Tests
{
    [TestClass]
    public class SitterCalculatorTests
    {
        SitterCalculator testCalc = new SitterCalculator();

        [TestMethod]
        public void ConvertInputTimeToMiltaryTimeFromString()
        {
            Assert.AreEqual(17, testCalc.TimeConversion("5:00 PM"));
            Assert.AreEqual(1, testCalc.TimeConversion("1:00 AM"));
            Assert.AreEqual(18, testCalc.TimeConversion("5:01 PM"));
            Assert.AreEqual(5, testCalc.TimeConversion("4:01 AM"));
        }

        [TestMethod]
        public void CheckStartAndEndTimeIsValid()
        {
            Assert.AreEqual(false, testCalc.CheckStartAndEndTime("5:00 AM"));
            Assert.AreEqual(false, testCalc.CheckStartAndEndTime("4:00 PM"));
            Assert.AreEqual(true, testCalc.CheckStartAndEndTime("11:00 PM"));
            Assert.AreEqual(true, testCalc.CheckStartAndEndTime("4:00 AM"));
            Assert.AreEqual(true, testCalc.CheckStartAndEndTime("5:00 PM"));
            Assert.AreEqual(true, testCalc.CheckStartAndEndTime("1:00 AM"));
        }

        //Assumption: Bedtime is always prior to midnight
        [TestMethod]
        public void CalculateMoniesEarnedFromStartTimeToBedTime()
        {
            Assert.AreEqual(36, testCalc.CalculateMoniesBeforeBedTime("5:00 PM", "8:00 PM"));
        }

        [TestMethod]
        public void CalculateMoniesEarnedFromBedTimeToMidnight()
        {
            Assert.AreEqual(32, testCalc.CalculateMoniesAfterBedTimeBeforeMidnight("8:00 PM", "1:00 AM"));
            Assert.AreEqual(16, testCalc.CalculateMoniesAfterBedTimeBeforeMidnight("9:00 PM", "11:00 PM"));
        }

        [TestMethod]
        public void CalculateMoniesFromMidnightToEndTime()
        {
            Assert.AreEqual(16, testCalc.CalculateMoniesFromMidnightToEndTime("1:00 AM"));
            Assert.AreEqual(0, testCalc.CalculateMoniesFromMidnightToEndTime("11:00 PM"));
        }

    }
}
