﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabySitterCalculator
{
    public class CLI
    {
        private CLIHelper helper;
        public CLI(CLIHelper helper)
        {
            this.helper = helper;
        }
        public void Run()
        {
            string userInputStart = "";
            string userInputBed = "";
            string userInputEnd = "";
            Console.WriteLine("Welcome BabySitter!");
            Console.WriteLine();
            Console.Write("Please enter your start time for this session (example 5:03 PM): ");
            userInputStart = Console.ReadLine();

            while (!helper.VerifyTimeFormat(userInputStart))
            {
                Console.Write("Sorry that start time is not valid, please re-enter your start time (example 5:09 PM): ");
                userInputStart = Console.ReadLine();
            }

            Console.Write("Please enter this sessions bed time (example 8:13 PM): ");
            userInputBed = Console.ReadLine();
            while (!helper.VerifyTimeFormat(userInputBed))
            {
                Console.Write("Sorry that bed time is not valid, please re-enter your bed time (example 9:19 PM): ");
                userInputBed = Console.ReadLine();
            }
            Console.Write("Please enter this sessions end time (example 11:00 PM): ");
            userInputEnd = Console.ReadLine();
            while (!helper.VerifyTimeFormat(userInputEnd))
            {
                Console.Write("Sorry that end time is not valid, please re-enter your end time (example 1:27 AM): ");
                userInputEnd = Console.ReadLine();
            }
            int moniesEarned = helper.CalculateMoniesEarnedFromCLIInputs(userInputStart, userInputBed, userInputEnd);
            Console.WriteLine("Thank you!");
            Console.WriteLine($"You will earn ${moniesEarned}.00 for this babysitting session.");
            Console.WriteLine();
            Console.WriteLine("Press enter to calculate a new session.");
            Console.ReadLine();
            Console.Clear();
            return;
        }
    }
}