﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabySitterCalculator
{
    public class CLIHelper
    {
        private SitterCalculator sitterCalculator;

        public CLIHelper(SitterCalculator sitterCalculator)
        {
            this.sitterCalculator = sitterCalculator;
        }

       public bool VerifyTimeFormat(string userInput)
        {
            if (userInput.Length < 6)
            {
                return false;
            }
            else if (!userInput.Contains(':'))
            {
                return false;
            }
            else if(!userInput.Contains("PM") && !userInput.Contains("AM"))
            {
                return false;
            }
            else if(!userInput.Contains(" "))
            {
                return false;
            }
            else if (!sitterCalculator.CheckStartAndEndTime(userInput))
            {
                return false;
            }
            return true;
        }

        public bool VerifyUserInputTimeIsValid(string userInput)
        {
            return sitterCalculator.CheckStartAndEndTime(userInput);
        }

        public int CalculateMoniesEarnedFromCLIInputs(string userStartTimeInput, string userBedTimeInput, string userEndTimeInput)
        {
            int totalMonies = sitterCalculator.CalculateMoniesBeforeBedTime(userStartTimeInput, userBedTimeInput);
            totalMonies += sitterCalculator.CalculateMoniesAfterBedTimeBeforeMidnight(userBedTimeInput, userEndTimeInput);
            totalMonies += sitterCalculator.CalculateMoniesFromMidnightToEndTime(userEndTimeInput);
            return totalMonies;
        }
    }
}
