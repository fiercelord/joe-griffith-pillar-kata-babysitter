﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabySitterCalculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            SitterCalculator sitterCalculator = new SitterCalculator();
            CLIHelper helper = new CLIHelper(sitterCalculator);
            CLI cli = new CLI(helper);
            while (true)
            {
                cli.Run();
            }
        }
    }
}
