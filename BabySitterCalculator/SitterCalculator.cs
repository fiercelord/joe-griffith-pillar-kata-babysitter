﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabySitterCalculator
{
    public class SitterCalculator
    {
        public int TimeConversion(string time)
        {
            int convertedTime = Convert.ToInt32(time.Substring(0, time.IndexOf(":")));
            int minutesWorked = Convert.ToInt32(time.Substring(time.IndexOf(":") + 1, 2));
            if (time.Contains("PM"))
            {
                convertedTime += 12;
            }
            if (minutesWorked > 0)
            {
                convertedTime += 1;
            }
            return convertedTime;
        }

        public bool CheckStartAndEndTime(string time)
        {
            int convertedTime = TimeConversion(time);
            if (convertedTime > 4 && convertedTime < 17)
            {
                return false;
            }
            return true;
        }

        public int CalculateMoniesBeforeBedTime(string startTime, string bedTime)
        {
            int convertedStartTime = TimeConversion(startTime);
            int convertedBedTime = TimeConversion(bedTime);
            return 12 * (convertedBedTime - convertedStartTime);
        }

        public int CalculateMoniesAfterBedTimeBeforeMidnight(string bedTime, string endTime)
        {
            int convertedEndTime = TimeConversion(endTime);
            int convertedBedTime = TimeConversion(bedTime);
            if (convertedEndTime <= 4)
            {
                return 8 * (24 - convertedBedTime);
            }
            else
            {
                return 8 * (convertedEndTime - convertedBedTime);
            }
        }

        public int CalculateMoniesFromMidnightToEndTime(string endTime)
        {
            int convertedEndTime = TimeConversion(endTime);
            if (convertedEndTime <= 4)
            {
                return 16 * (convertedEndTime);
            }
            return 0;
        }
    }
}
